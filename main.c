#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>

#define M_PI 3.14159265358979323846

typedef uint8_t  u8;
typedef uint32_t u32;
typedef double   f64;

void
error(u8 *txt) {
	fprintf(stderr, "ERROR: %s\n", txt);
	exit(-1);
}

typedef enum {
	TOK_TYPE_NUM,
	TOK_TYPE_PLUS,
	TOK_TYPE_MINUS,
	TOK_TYPE_STAR,
	TOK_TYPE_SLASH,
	TOK_TYPE_CARET,
	TOK_TYPE_LPAREN,
	TOK_TYPE_RPAREN,
	TOK_TYPE_SIN,
	TOK_TYPE_COS,
	TOK_TYPE_SQRT,
	TOK_TYPE_EOF,
} ttype;

typedef struct {
	u32    len;
	u32    cap;
	ttype *types;
	f64   *vals;
} tokens;

void
tokens_init(tokens *list) {
	list->cap   = 4;
	list->len   = 0;
	list->types = malloc(sizeof (ttype) * list->cap);
	if (list->types == NULL)
		error("could not allocate memory for [types].");

	list->vals  = malloc(sizeof (f64) * list->cap);
	if (list->vals == NULL) {
		free(list->types);
		error("could not allocate memory for [values].");
	}
}

void
tokens_gen(tokens *list, u8 *txt) {
	ttype t;
	f64   v;

	while (true) {
		while (*txt == ' ' || *txt == '\n' || *txt == '\t')
			++txt;

		if (*txt >= '0' && *txt <= '9') {
			t = TOK_TYPE_NUM;		
		
			v = *txt - '0';
			++txt;

			while (*txt >= '0' && *txt <= '9') {
				v *= 10;
				v += *txt - '0';

				++txt;
			}

			if (*txt == '.') {
				++txt;

				// 0.567
				// 0.5

				f64 w;
				w = 0.1;
				while (*txt >= '0' && *txt <= '9') {
					v += (*txt - '0') * w;
					w *= 0.1;

					++txt;
				}
			}				

			//putting back the next char.
			--txt;
		} else if (*txt == 'p') {
			++txt;
			if (*txt != 'i')
				error("syntax error, probably you want to write [pi].");

			t = TOK_TYPE_NUM;
			v = M_PI;
		} else if (*txt == 'e') {
			t = TOK_TYPE_NUM;
			v = exp(1.0);
		} else if (*txt == '+') {
			t = TOK_TYPE_PLUS;
		} else if (*txt == '-') {
			t = TOK_TYPE_MINUS;
		} else if (*txt == '*') {
			t = TOK_TYPE_STAR;
		} else if (*txt == '/') {
			t = TOK_TYPE_SLASH;
		} else if (*txt == '^') {
			t = TOK_TYPE_CARET; 
		} else if (*txt == '(') {
			t = TOK_TYPE_LPAREN;
		} else if (*txt == ')') {
			t = TOK_TYPE_RPAREN;
		} else if (*txt == 's') {
			++txt;
			if (*txt == 'i') {
				++txt;
				if (*txt == 'n')
					t = TOK_TYPE_SIN;
					
				error("syntax error, probably you want to write [sin].");
			} else if (*txt == 'q') {
				++txt;
				if (*txt != 'r')
					error("syntax error, probably you want to write [sqrt].");

				++txt;
				if (*txt != 't')
					error("syntax error, probably you want to write [sqrt].");

				t = TOK_TYPE_SQRT;
			} else {
				error("syntax error, probably you want to write [sin] or [sqrt].");
			}
		} else if (*txt == 'c') {
			++txt;
			if (*txt != 'o')
				error("syntax error, probably you want to write [cos].");
			++txt;
			if (*txt != 's')
				error("syntax error, probably you want to write [cos].");

			t = TOK_TYPE_COS;
		} else if (*txt == '\0') {
			t = TOK_TYPE_EOF;
		} else {
			error("syntax error.");
		}

		++txt;

		if (list->len == list->cap) {
			list->cap   *= 2;
			list->types  = realloc(list->types, sizeof (ttype) * list->cap);
			if (list->types == NULL)
				error("could not reallocate memory for [types].");

			list->vals = realloc(list->vals, sizeof (f64) * list->cap);
			if (list->vals == NULL) {
				free(list->types);
				error("could not reallocate memory for [values].");
			}
		}

		list->types[list->len] = t;

		if (t == TOK_TYPE_NUM)
			list->vals[list->len] = v;

		++list->len;

		if (t == TOK_TYPE_EOF)
			break;
	}
}

void
tokens_print(tokens *list) {
	printf("================================= TOKENS =================================\n");

	for (u32 i = 0; i < list->len; ++i) {
		switch (list->types[i]) {
		case TOK_TYPE_NUM:
			printf("TTYPE: NUMBER, VAL: %.2f\n", list->vals[i]);
			break;
		case TOK_TYPE_PLUS:
			printf("TTYPE: PLUS\n");
			break;
		case TOK_TYPE_MINUS:
			printf("TTYPE: MINUS\n");
			break;
		case TOK_TYPE_STAR:
			printf("TTYPE: STAR\n");
			break;
		case TOK_TYPE_CARET:
			printf("TTYPE: CARET\n");
			break;
		case TOK_TYPE_SLASH:
			printf("TTYPE: SLASH\n");
			break;
		case TOK_TYPE_LPAREN:
			printf("TTYPE: LEFT PAREN\n");
			break;
		case TOK_TYPE_RPAREN:
			printf("TTYPE: RIGHT PAREN\n");
			break;
		case TOK_TYPE_SIN:
			printf("TTYPE: SIN\n");
			break;
		case TOK_TYPE_COS:
			printf("TTYPE: COS\n");
			break;
		case TOK_TYPE_SQRT:
			printf("TTYPE: SQRT\n");
			break;
		case TOK_TYPE_EOF:
			printf("TTYPE: EOF\n");
			break;
		}
	}
}

void
tokens_free(tokens *list) {
	free(list->vals);
	free(list->types);
}

typedef enum {
	NODE_TYPE_NUM  = TOK_TYPE_NUM,
	NODE_TYPE_ADD  = TOK_TYPE_PLUS,
	NODE_TYPE_SUB  = TOK_TYPE_MINUS,
	NODE_TYPE_MUL  = TOK_TYPE_STAR,
	NODE_TYPE_DIV  = TOK_TYPE_SLASH,
	NODE_TYPE_POW  = TOK_TYPE_CARET,
	NODE_TYPE_SIN  = TOK_TYPE_SIN,
	NODE_TYPE_COS  = TOK_TYPE_COS,
	NODE_TYPE_SQRT = TOK_TYPE_SQRT,
} ntype; 

typedef struct node node;
struct node {
	ntype  type;
	node  *left;
	node  *right;
	f64    val;
};

node*
tree_prior_3(tokens*, u32*);

node*
tree_prior_1(tokens *list, u32 *cursor) {
	node *left;
	left = malloc(sizeof (node));
	left->left  = NULL;
	left->right = NULL;


	if (list->types[*cursor] == TOK_TYPE_LPAREN) {
		++(*cursor);
		left = tree_prior_3(list, cursor);
		
		if (list->types[*cursor] != TOK_TYPE_RPAREN)
			error("missing right parenthesis.");

		++(*cursor);
		return left;
	} else if (list->types[*cursor] == TOK_TYPE_PLUS || list->types[*cursor] == TOK_TYPE_MINUS || 
		   list->types[*cursor] == TOK_TYPE_SIN  || list->types[*cursor] == TOK_TYPE_COS   ||
		   list->types[*cursor] == TOK_TYPE_SQRT) {
		left->type = list->types[*cursor];

		++(*cursor);
		left->right = tree_prior_1(list, cursor);

		left->left = malloc(sizeof (node));
		left->left->type  = NODE_TYPE_NUM;
		left->left->val   = 0.0;
		left->left->left  = NULL;
		left->left->right = NULL;

		return left;
	} else if (list->types[*cursor] == TOK_TYPE_NUM) {
		left->type = NODE_TYPE_NUM;
		left->val  = list->vals[*cursor];

		++(*cursor);
		return left;
	}

	error("parse tree error.");
}

node*
tree_prior_2(tokens *list, u32 *cursor) {
	node *left;
	left = tree_prior_1(list, cursor);

	node *temp;
	while (list->types[*cursor] != TOK_TYPE_EOF  &&
	      (list->types[*cursor] == TOK_TYPE_STAR || list->types[*cursor] == TOK_TYPE_SLASH || list->types[*cursor] == TOK_TYPE_CARET)) {
		node *temp;
		temp = malloc(sizeof (node));

		temp->type = list->types[*cursor];
		temp->left = left;

		++(*cursor);
		temp->right = tree_prior_1(list, cursor);

		left = temp;
	}

	return left;
}

node*
tree_prior_3(tokens *list, u32 *cursor) {
	node *left;
	left = tree_prior_2(list, cursor);

	node *temp;
	while (list->types[*cursor] != TOK_TYPE_EOF  && 
	      (list->types[*cursor] == TOK_TYPE_PLUS || list->types[*cursor] == TOK_TYPE_MINUS)) {
		temp = malloc(sizeof (node));

		temp->type  = list->types[*cursor];
		temp->left  = left;

		++(*cursor);			
		temp->right = tree_prior_2(list, cursor);

		left = temp;
	}

	return left;
}

node*
tree_parse(tokens *list) {
	u32 cursor;
	cursor = 0;

	node *root;
	root = tree_prior_3(list, &cursor);

	return root;
}

void
node_print(node *root, u32 tabs) {
	for (u32 i = 0; i < tabs; ++i)
		printf("--");
	printf("> ");

	++tabs;

	switch (root->type) {
	case NODE_TYPE_NUM:
		printf("NTYPE: NUMBER, VAL: %.2f\n", root->val);
		break;
	case NODE_TYPE_ADD:
		printf("NTYPE: ADD\n");
		break;
	case NODE_TYPE_SUB:
		printf("NTYPE: SUB\n");
		break;
	case NODE_TYPE_MUL:
		printf("NTYPE: MUL\n");
		break;
	case NODE_TYPE_DIV:
		printf("NTYPE: DIV\n");
		break;
	case NODE_TYPE_POW:
		printf("NTYPE: POW\n");
		break;
	case NODE_TYPE_SIN:
		printf("NTYPE: SIN\n");
		break;
	case NODE_TYPE_COS:
		printf("NTYPE: COS\n");
		break;
	case NODE_TYPE_SQRT:
		printf("NTYPE: SQRT\n");
		break;
	}

	if (root->left != NULL)
		node_print(root->left, tabs);
	if (root->right != NULL)
		node_print(root->right, tabs);
}

void
tree_print(node *root) {
	printf("================================= NODES =================================\n");

	u32 tabs;
	tabs = 0;

	node_print(root, tabs);
}

f64
tree_interp(node *root) {
	switch (root->type) {
	case NODE_TYPE_ADD:
		return tree_interp(root->left) + tree_interp(root->right);
	case NODE_TYPE_SUB:
		return tree_interp(root->left) - tree_interp(root->right);
	case NODE_TYPE_MUL:
		return tree_interp(root->left) * tree_interp(root->right);
	case NODE_TYPE_DIV:
		return tree_interp(root->left) / tree_interp(root->right);
	case NODE_TYPE_POW:
		return pow(tree_interp(root->left), tree_interp(root->right));
	case NODE_TYPE_SIN:
		return sin(tree_interp(root->right));
	case NODE_TYPE_COS:
		return cos(tree_interp(root->right));
	case NODE_TYPE_SQRT:
		return sqrt(tree_interp(root->right));
	case NODE_TYPE_NUM:
		return root->val;
	}
}

void
tree_free(node *root) {
	if (root->left != NULL)
		tree_free(root->left);

	if (root->right != NULL)
		tree_free(root->right);

	free(root);
}

int
main(int argc, char **args) {
	u8 *expr = "sqrt(27 / 3 + 0.0000 * 1)";

	tokens list;
	tokens_init(&list);
	tokens_gen(&list, expr);
	tokens_print(&list);

	node *root;
	root = tree_parse(&list);
	tree_print(root);

	f64 res;
	res = tree_interp(root);
	printf("RES: %.2f", res);

	tree_free(root);
	tokens_free(&list);

	return 0;
}
